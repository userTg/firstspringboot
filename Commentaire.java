package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Commentaire {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String libelle;
	private String subject;
	private String description;
	private String date;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Commentaire(int id, String libelle, String subject, String description, String date) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.subject = subject;
		this.description = description;
		this.date = date;
	}

	public Commentaire() {
		super();
	}

	@Override
	public String toString() {
		return "Commentaire [id=" + id + ", libelle=" + libelle + ", subject=" + subject + ", description="
				+ description + ", date=" + date + "]";
	}

}
